package main

import (
	"gitlab.com/mvenezia/cma-vmware/cmd/cma-vmware/cmd"
)

func main() {
	cmd.Execute()
}
